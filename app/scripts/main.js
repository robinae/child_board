(function ($) {
    // Detect touch support
    $.support.touch = 'ontouchend' in document;
    // Ignore browsers without touch support
    if (!$.support.touch) {
    return;
    }
    var mouseProto = $.ui.mouse.prototype,
        _mouseInit = mouseProto._mouseInit,
        touchHandled;

    function simulateMouseEvent (event, simulatedType) { //use this function to simulate mouse event
    // Ignore multi-touch events
        if (event.originalEvent.touches.length > 1) {
        return;
        }
    event.preventDefault(); //use this to prevent scrolling during ui use

    var touch = event.originalEvent.changedTouches[0],
        simulatedEvent = document.createEvent('MouseEvents');
    // Initialize the simulated mouse event using the touch event's coordinates
    simulatedEvent.initMouseEvent(
        simulatedType,    // type
        true,             // bubbles                    
        true,             // cancelable                 
        window,           // view                       
        1,                // detail                     
        touch.screenX,    // screenX                    
        touch.screenY,    // screenY                    
        touch.clientX,    // clientX                    
        touch.clientY,    // clientY                    
        false,            // ctrlKey                    
        false,            // altKey                     
        false,            // shiftKey                   
        false,            // metaKey                    
        0,                // button                     
        null              // relatedTarget              
        );

    // Dispatch the simulated event to the target element
    event.target.dispatchEvent(simulatedEvent);
    }
    mouseProto._touchStart = function (event) {
    var self = this;
    // Ignore the event if another widget is already being handled
    if (touchHandled || !self._mouseCapture(event.originalEvent.changedTouches[0])) {
        return;
        }
    // Set the flag to prevent other widgets from inheriting the touch event
    touchHandled = true;
    // Track movement to determine if interaction was a click
    self._touchMoved = false;
    // Simulate the mouseover event
    simulateMouseEvent(event, 'mouseover');
    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');
    // Simulate the mousedown event
    simulateMouseEvent(event, 'mousedown');
    };

    mouseProto._touchMove = function (event) {
    // Ignore event if not handled
    if (!touchHandled) {
        return;
        }
    // Interaction was not a click
    this._touchMoved = true;
    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');
    };
    mouseProto._touchEnd = function (event) {
    // Ignore event if not handled
    if (!touchHandled) {
        return;
    }
    // Simulate the mouseup event
    simulateMouseEvent(event, 'mouseup');
    // Simulate the mouseout event
    simulateMouseEvent(event, 'mouseout');
    // If the touch interaction did not move, it should trigger a click
    if (!this._touchMoved) {
      // Simulate the click event
      simulateMouseEvent(event, 'click');
    }
    // Unset the flag to allow other widgets to inherit the touch event
    touchHandled = false;
    };
    mouseProto._mouseInit = function () {
    var self = this;
    // Delegate the touch handlers to the widget's element
    self.element
        .on('touchstart', $.proxy(self, '_touchStart'))
        .on('touchmove', $.proxy(self, '_touchMove'))
        .on('touchend', $.proxy(self, '_touchEnd'));

    // Call the original $.ui.mouse init method
    _mouseInit.call(self);
    };
})(jQuery);


jQuery(function(){

	var tasks = new Tasks();
	tasks.init();

	jQuery('[data-toggle="tooltip"]').tooltip()

});

function Tasks(){

	var $this = this;

	$this.items = (localStorage.getItem("my_list") ? JSON.parse(localStorage.getItem("my_list")) : [] );

	$this.init = function(){
		
		jQuery(".item").droppable({
	        accept: '.icon',
	        drop: function(event, ui) {

	        	$this.addItem( jQuery(this).index() , jQuery(ui.draggable).data().id , jQuery(this).parents("tr").data().type);
	            jQuery(this).append(jQuery(ui.draggable).clone());
	        }
	    });
	    jQuery(".tools .icon").draggable({
	        helper: 'clone'
	    });

	    jQuery(".item").on("click",".icon",function(){
	    	$this.removeItem( jQuery(this).parent(".item").index() , jQuery(this).data().id , jQuery(this).parents("tr").data().type);
    		jQuery(this).remove();
	    });

	    jQuery(".reset-all").on("click" , function(){

	    	$this.clear();

	    });

	    $this.start();
	}

	$this.addItem = function( day, value , type){
		var item = {
			id : value,
			day: day,
			type: type
		};

		$this.items.push(item);
		$this.update();
	}

	$this.removeItem = function( day , value , type ){
		jQuery($this.items).each(function(el,index){
			if(index.type == type && index.day == day && index.type == type){
				$this.items.splice(el,1);
				return false;
			}
		});
		$this.update();
	}

	$this.start = function(){
		//write
		
		jQuery($this.items).each(function(el,index){
			var line = jQuery("[data-type='"+ index.type + "']");
			var obj = jQuery("[data-id='"+ index.id + "']",".tools").clone();
			line.children("td").eq(index.day).append( obj );
		})
	}

	$this.update = function(){
		localStorage.setItem("my_list", JSON.stringify($this.items) );
	}

	$this.clear = function(){
		$this.items = [];
		$this.update();

		jQuery(".item .icon").remove();

	}

}